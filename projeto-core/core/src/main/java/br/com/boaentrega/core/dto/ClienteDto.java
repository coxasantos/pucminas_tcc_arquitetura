package br.com.boaentrega.core.dto;

import br.com.boaentrega.core.enumeration.SituacaoClienteEnum;
import br.com.boaentrega.core.enumeration.TipoClienteEnum;
import lombok.Data;

@Data
public class ClienteDto implements BaseDto {

	private static final long serialVersionUID = 7123101005630123635L;

	private Long id;

	private TipoClienteEnum tipo;

	private SituacaoClienteEnum situacao;

	private EnderecoDto endereco;

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
