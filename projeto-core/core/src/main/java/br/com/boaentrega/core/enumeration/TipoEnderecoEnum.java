package br.com.boaentrega.core.enumeration;

import java.util.Arrays;

import lombok.Getter;

@Getter
public enum TipoEnderecoEnum {

	COMERCIAL(1, "COMERCIAL"), CD(2, "CENTRO DE DISTRIBUIÇÃO");

	private Integer codigo;

	private String nome;

	private TipoEnderecoEnum(int codigo, String nome) {
		this.codigo = codigo;
		this.nome = nome;
	}

	public static TipoEnderecoEnum obterPorCodigo(Integer codigo) {
		if (codigo != null) {
			return Arrays.stream(TipoEnderecoEnum.values()).filter(s -> s.getCodigo().compareTo(codigo) == 0)
					.findFirst()
					.orElseThrow(() -> new IllegalStateException(String.format("Unsupported type %s.", codigo)));
		}
		return null;
	}
}
