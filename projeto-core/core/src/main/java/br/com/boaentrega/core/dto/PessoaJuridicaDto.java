package br.com.boaentrega.core.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.Data;

@Data
@JsonRootName(value = "Cliente")
public class PessoaJuridicaDto extends ClienteDto implements BaseDto {

	private static final long serialVersionUID = 72638736;

	private String cnpj;

	private String razaoSocial;

	private String nomeFantasia;

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

}
