package br.com.boaentrega.core.dto.indicador;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.boaentrega.core.dto.BaseDto;
import lombok.Data;

@Data
public class IndicadorPedidoEntreguePorClienteDto implements BaseDto {

	private static final long serialVersionUID = 181549133501866813L;

	@JsonProperty("data")
	private List<ItemPedidoEntreguePorClienteDto> itens;

}
