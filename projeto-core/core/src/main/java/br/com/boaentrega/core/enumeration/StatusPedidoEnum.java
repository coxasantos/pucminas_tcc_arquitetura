package br.com.boaentrega.core.enumeration;

import lombok.Getter;

@Getter
public enum StatusPedidoEnum {

	ABERTO(1, "Aberto"), EM_FATURAMENTO(2, "Em Faturamento"), SEPARADO_ENTREGA(3, "Separado  para Entrega"),
	EM_ROTA(4, "Em rota"), ENTREGUE(5, "Entregue"), DEVOLVIDO(6, "Devolvido"), CANCELADO(7, "Cancelado");

	private Integer codigo;

	private String valor;

	private StatusPedidoEnum(Integer codigo, String valor) {
		this.codigo = codigo;
		this.valor = valor;
	}
}
