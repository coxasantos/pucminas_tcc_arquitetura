package br.com.boaentrega.core.enumeration;

import java.util.Arrays;

import lombok.Getter;

@Getter
public enum SituacaoClienteEnum {

	ATIVO("S"), INATIVO("N");

	private String valor;

	private SituacaoClienteEnum(String valor) {
		this.valor = valor;
	}

	public static SituacaoClienteEnum obterPorValor(String valor) {
		if (valor != null) {
			return Arrays.stream(SituacaoClienteEnum.values()).filter(s -> s.getValor().equals(valor)).findFirst()
					.orElseThrow(() -> new IllegalStateException(String.format("Unsupported type %s.", valor)));
		}
		return null;
	}

}
