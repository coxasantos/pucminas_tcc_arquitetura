package br.com.boaentrega.core.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

import br.com.boaentrega.core.enumeration.StatusPedidoEnum;
import lombok.Data;

@Data
@JsonRootName(value = "Pedido")
public class PedidoDto implements BaseDto {

	private static final long serialVersionUID = 3533465304473118031L;

	private Long id;

	private Long idCliente;

	private StatusPedidoEnum status;

}
