package br.com.boaentrega.core.dto;

import br.com.boaentrega.core.enumeration.TipoEnderecoEnum;
import lombok.Data;

@Data
public class EnderecoDto implements BaseDto {

	private static final long serialVersionUID = -4913209221387501569L;

	private Long id;

	private TipoEnderecoEnum tipo;

	private Integer cep;

	private String uf;

	private String logradouro;

	private String numero;

	private String complemento;

	private String bairro;

	private String cidade;

	private String pais;

	private Double latitude;

	private Double longitude;

}
