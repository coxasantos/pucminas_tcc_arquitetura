package br.com.boaentrega.core.mapper;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

public interface GenericModelMapper {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T, O, S, D> T transform(O obj, Class<T> objClass, List<PropertyMap> propertyMaps) {
		ModelMapper modalMapper = new ModelMapper();
		if (propertyMaps != null) {
			for (PropertyMap<S, D> propertyMap : propertyMaps) {
				modalMapper.addMappings(propertyMap);
			}
		}
		return modalMapper.map(obj, objClass);
	}

}
