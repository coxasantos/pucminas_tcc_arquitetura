package br.com.boaentrega.core.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.Data;

@Data
@JsonRootName(value = "Cliente")
public class PessoaFisicaDto extends ClienteDto implements BaseDto {

	private static final long serialVersionUID = -868102885969342343L;

	private String nome;

	private String cpf;

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
