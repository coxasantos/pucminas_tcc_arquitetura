package br.com.boaentrega.core.enumeration;

import java.util.Arrays;

import lombok.Getter;

@Getter
public enum TipoClienteEnum {

	CENTRO_DISTRIBUICAO(1), DESTINATARIO(2), REMETENTE(3);

	private Integer codigo;

	private TipoClienteEnum(Integer codigo) {
		this.codigo = codigo;
	}

	public static TipoClienteEnum obterPorCodigo(Integer codigo) {
		if (codigo != null) {
			return Arrays.stream(TipoClienteEnum.values()).filter(s -> s.getCodigo().compareTo(codigo) == 0).findFirst()
					.orElseThrow(() -> new IllegalStateException(String.format("Unsupported type %s.", codigo)));
		}
		return null;
	}

}
