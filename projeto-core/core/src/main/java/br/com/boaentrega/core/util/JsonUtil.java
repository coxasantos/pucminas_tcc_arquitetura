package br.com.boaentrega.core.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import br.com.boaentrega.core.dto.BaseDto;

public class JsonUtil {

	private JsonUtil() {
		super();
	}

	public static String gerarJson(BaseDto dto) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
		return objectMapper.writeValueAsString(dto);
	}

	public static <T> T gerarDto(String json, Class<T> valueType) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);
		return objectMapper.readValue(json, valueType);
	}

}
