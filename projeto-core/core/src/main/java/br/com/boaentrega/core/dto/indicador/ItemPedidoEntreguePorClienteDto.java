package br.com.boaentrega.core.dto.indicador;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ItemPedidoEntreguePorClienteDto implements Serializable {

	private static final long serialVersionUID = -9108131297652914054L;

	@JsonProperty("cnpj_cliente")
	private String cnpj;

	@JsonProperty("qtde_pedidos_entregues")
	private Long quantidade;

	@JsonProperty("razao_social")
	private String razaoSocial;

}
