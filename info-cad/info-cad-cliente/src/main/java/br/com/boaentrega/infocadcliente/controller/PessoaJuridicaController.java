package br.com.boaentrega.infocadcliente.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
//import com.fasterxml.jackson.databind.ObjectMapper; 
//import com.fasterxml.jackson.databind.ObjectWriter;

import br.com.boaentrega.core.util.JsonUtil;
import br.com.boaentrega.infocadcliente.exception.ResourceNotFoundException;
import br.com.boaentrega.infocadcliente.message.QueueSender;
import br.com.boaentrega.infocadcliente.model.entity.PessoaJuridicaEntity;
import br.com.boaentrega.infocadcliente.repository.PessoaJuridicaRepository;

@RestController
@RequestMapping(value = { "/info-fornecedor/api/v1" }, produces = { "application/json" })
public class PessoaJuridicaController {

	@Autowired
	private PessoaJuridicaRepository pessoaJuridicaRepository;

	@Autowired
	private QueueSender queueSender;

	@GetMapping("/pessoaJuridica")
	public List<PessoaJuridicaEntity> getAllpessoaJuridica() {
		return pessoaJuridicaRepository.findAll();
	}

	@RolesAllowed("user")
	@GetMapping("/pessoaJuridica/{id}")
	public ResponseEntity<PessoaJuridicaEntity> getPessoaJuridicabyEntity(
			@PathVariable(value = "id") Long pessoaJuridicaId) throws ResourceNotFoundException {
		PessoaJuridicaEntity PessoaJuridica = pessoaJuridicaRepository.findById(pessoaJuridicaId).orElseThrow(
				() -> new ResourceNotFoundException("pessoaJuridica not found for this id :: " + pessoaJuridicaId));
		return ResponseEntity.ok().body(PessoaJuridica);
	}
	

	@PostMapping("/pessoaJuridica")
	public PessoaJuridicaEntity createPessoaJuridica(@Valid @RequestBody PessoaJuridicaEntity PessoaJuridica) {
		//queueSender.send(JsonUtil.gerarJson(clienteDto), "cliente");
		return pessoaJuridicaRepository.save(PessoaJuridica);
	}

	@PutMapping("/pessoaJuridica/{id}")
	public ResponseEntity<PessoaJuridicaEntity> updatePessoaJuridica(@PathVariable(value = "id") Long pessoaJuridicaId,
			@Valid @RequestBody PessoaJuridicaEntity pessoaJuridicaDetails) throws ResourceNotFoundException {
		PessoaJuridicaEntity PessoaJuridica = pessoaJuridicaRepository.findById(pessoaJuridicaId).orElseThrow(
				() -> new ResourceNotFoundException("pessoaJuridica not found for this id :: " + pessoaJuridicaId));

		PessoaJuridica.setCnpj(pessoaJuridicaDetails.getCnpj());
		PessoaJuridica.setNomeFantasia(pessoaJuridicaDetails.getNomeFantasia());
		PessoaJuridica.setRazaoSocial(pessoaJuridicaDetails.getRazaoSocial());
		final PessoaJuridicaEntity updatedPessoaJuridica = pessoaJuridicaRepository.save(PessoaJuridica);
		return ResponseEntity.ok(updatedPessoaJuridica);
	}

	@DeleteMapping("/pessoaJuridica/{id}")
	public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long pessoaJuridicaId)
			throws ResourceNotFoundException {
		PessoaJuridicaEntity PessoaJuridica = pessoaJuridicaRepository.findById(pessoaJuridicaId).orElseThrow(
				() -> new ResourceNotFoundException("pessoaJuridica not found for this id :: " + pessoaJuridicaId));

		pessoaJuridicaRepository.delete(PessoaJuridica);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

}
