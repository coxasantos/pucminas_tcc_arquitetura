package br.com.boaentrega.infocadcliente.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import br.com.boaentrega.infocadcliente.model.entity.EnderecoClienteEntity;

@Repository
public interface  EnderecoClienteRepository extends JpaRepository<EnderecoClienteEntity, Long>{
    
}
