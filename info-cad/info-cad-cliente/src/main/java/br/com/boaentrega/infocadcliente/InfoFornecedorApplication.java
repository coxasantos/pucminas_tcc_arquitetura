package br.com.boaentrega.infocadcliente;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;

@EnableRabbit
@SpringBootApplication
public class InfoFornecedorApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfoFornecedorApplication.class, args);
	}

}