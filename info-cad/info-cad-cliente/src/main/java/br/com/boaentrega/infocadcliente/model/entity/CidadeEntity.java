package br.com.boaentrega.infocadcliente.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "CID_CIDADE")
public class CidadeEntity {

	@Id
	@Column(name = "CID_ID")
	private Long id;

	@Column(name = "CID_NM_CIDADE")
	private String nome;

}