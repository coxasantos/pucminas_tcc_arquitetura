package br.com.boaentrega.infocadcliente.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import br.com.boaentrega.infocadcliente.model.entity.PessoaJuridicaEntity;

@Repository
public interface  PessoaJuridicaRepository extends JpaRepository<PessoaJuridicaEntity, Long>{
    
}