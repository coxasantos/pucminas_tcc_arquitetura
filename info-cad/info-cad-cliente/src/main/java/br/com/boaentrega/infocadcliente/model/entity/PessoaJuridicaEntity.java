package br.com.boaentrega.infocadcliente.model.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;
import lombok.Data;

@Data
@Entity
@Table(name = "PJU_PESSOA_JURIDICA")
public class PessoaJuridicaEntity //implements Serializable 
{

	//private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PJU_ID", nullable = false)
	private Long id;

	@Column(name = "PJU_RAZAO_SOCIAL", nullable = false)
	private String razaoSocial;

	@Column(name = "PJU_NOME_FANTASIA", nullable = false)
	private String nomeFantasia;

	@Column(name = "PJU_NR_CNPJ", nullable = false)
	private String cnpj;

	@OneToOne
	@JoinColumn(name = "PJU_ID")
	private ClienteEntity cliente; 


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaJuridicaEntity other = (PessoaJuridicaEntity) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

}
