package br.com.boaentrega.infocadcliente.model.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.boaentrega.core.enumeration.SituacaoClienteEnum;
import br.com.boaentrega.core.enumeration.TipoClienteEnum;
import br.com.boaentrega.infocadcliente.model.converter.SituacaoClienteConverter;
import br.com.boaentrega.infocadcliente.model.converter.TipoClienteConverter;
import lombok.Data;

@Data
@Entity
@Table(name = "CLI_CLIENTE")
public class ClienteEntity {

	@Id
	@Column(name = "CLI_ID")
	private Long id;

	@Convert(converter = TipoClienteConverter.class)
	@Column(name = "TCL_ID")
	private TipoClienteEnum tipo;

	@Convert(converter = SituacaoClienteConverter.class)
	@Column(name = "CLI_ST_SITUACAO")
	private SituacaoClienteEnum situacao;

	//@OneToOne
	//private EnderecoClienteEntity endereco;

	@OneToOne
	@JoinColumn(name = "CLI_ID")	
	private EnderecoClienteEntity endereco;

	//@Override
	//public boolean equals(Object obj) {
	//	if (this == obj)
	//		return true;
	//	if (obj == null)
	//		return false;
	//	if (getClass() != obj.getClass())
	//		return false;
	//	ClienteEntity other = (ClienteEntity) obj;
	//	return Objects.equals(id, other.id);
	//}

	//@Override
	//public int hashCode() {
	//	return Objects.hash(id);
	//}

}
