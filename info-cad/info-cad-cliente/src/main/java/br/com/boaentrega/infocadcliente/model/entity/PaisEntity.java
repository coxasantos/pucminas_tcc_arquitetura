package br.com.boaentrega.infocadcliente.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Data
@Entity
@Table(name = "PAI_PAIS")
public class PaisEntity {
	
	@Id
	@Column(name="PAI_ID")
	private Long id;
	
	@Column(name="PAI_NM_PAIS")
	private String nome;
	
	

}
