package br.com.boaentrega.infocadcliente.config.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.PropertyMap;

import br.com.boaentrega.infocadcliente.model.entity.PessoaJuridicaEntity;
import br.com.boaentrega.core.dto.PessoaJuridicaDto;
import br.com.boaentrega.core.mapper.GenericModelMapper;

public class PessoaJuridicaMapper {

	private PessoaJuridicaMapper() {
		super();
	}

	@SuppressWarnings("rawtypes")
	public static PessoaJuridicaDto transformar(PessoaJuridicaEntity pessoaJuridicaEntity) {
		List<PropertyMap> lista = new ArrayList<>();
		lista.add(ClienteMapper.getPropertyMap());
		//lista.add(getPropertyMap());
		return GenericModelMapper.transform(pessoaJuridicaEntity, PessoaJuridicaDto.class, lista);

	}

	public static List<PessoaJuridicaDto> transformar(List<PessoaJuridicaEntity> pessoasJuridicas) {
		List<PessoaJuridicaDto> pessoaJuridicasDto = new ArrayList<>();
		pessoasJuridicas.forEach(pessoaJuridica -> pessoaJuridicasDto.add(PessoaJuridicaMapper.transformar(pessoaJuridica)));
		return pessoaJuridicasDto;
	}

	//protected static PropertyMap<PessoaJuridicaEntity, PessoaJuridicaDto> getPropertyMap() {
	//	return new PropertyMap<PessoaJuridicaEntity, PessoaJuridicaDto>() {
	//		protected void configure() {
	//			map().setCnpj(source.getCnpj());
      //          map().setNomeFantasia(source.getNomeFantasia());
        //        map().setRazaoSocial(source.getRazaoSocial());
		//	}
		//};
	//}

}
