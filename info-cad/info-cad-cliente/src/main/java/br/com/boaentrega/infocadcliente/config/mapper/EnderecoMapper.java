package br.com.boaentrega.infocadcliente.config.mapper;

import org.modelmapper.PropertyMap;

import br.com.boaentrega.infocadcliente.model.entity.EnderecoClienteEntity;
import br.com.boaentrega.core.dto.EnderecoDto;
import br.com.boaentrega.core.mapper.GenericModelMapper;

import java.util.ArrayList;
import java.util.List;

public class EnderecoMapper {

	private EnderecoMapper() {
		super();
	}

	@SuppressWarnings("rawtypes")
	public static EnderecoDto transformar(EnderecoClienteEntity enderecoClienteEntity) {
		List<PropertyMap> lista = new ArrayList<>();
		//lista.add(getPropertyMap());
		return GenericModelMapper.transform(enderecoClienteEntity, EnderecoDto.class, lista);

	}

	public static List<EnderecoDto> transformar(List<EnderecoClienteEntity> enderecos) {
		List<EnderecoDto> enderecosDto = new ArrayList<>();
		enderecos.forEach(endereco -> enderecosDto.add(EnderecoMapper.transformar(endereco)));
		return enderecosDto;
	}


		protected static PropertyMap<EnderecoClienteEntity, EnderecoDto> getPropertyMap() {
			return new PropertyMap<EnderecoClienteEntity, EnderecoDto>() {
				protected void configure() {
	
				}
			};
		}

	}

