package br.com.boaentrega.infocadcliente.config.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.PropertyMap;

import br.com.boaentrega.infocadcliente.model.entity.ClienteEntity;
import br.com.boaentrega.core.dto.ClienteDto;
import br.com.boaentrega.core.mapper.GenericModelMapper;

public class ClienteMapper {

	private ClienteMapper() {
		super();
	}

	@SuppressWarnings("rawtypes")
	public static ClienteDto transformar(ClienteEntity clienteEntity) {
		List<PropertyMap> lista = new ArrayList<>();
		lista.add(EnderecoMapper.getPropertyMap());
		//lista.add(getPropertyMap());
		return GenericModelMapper.transform(clienteEntity, ClienteDto.class, lista);

	}

	public static List<ClienteDto> transformar(List<ClienteEntity> clientes) {
		List<ClienteDto> clientesDto = new ArrayList<>();
		clientes.forEach(cliente -> clientesDto.add(ClienteMapper.transformar(cliente)));
		return clientesDto;
	}

	protected static PropertyMap<ClienteEntity, ClienteDto> getPropertyMap() {
		return new PropertyMap<ClienteEntity, ClienteDto>() {
			protected void configure() {

			}
		};
	}

}
