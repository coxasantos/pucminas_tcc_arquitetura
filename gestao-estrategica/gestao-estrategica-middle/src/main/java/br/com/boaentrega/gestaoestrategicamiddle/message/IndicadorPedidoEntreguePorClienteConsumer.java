package br.com.boaentrega.gestaoestrategicamiddle.message;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.boaentrega.core.dto.indicador.IndicadorPedidoEntreguePorClienteDto;
import br.com.boaentrega.core.util.JsonUtil;
import br.com.boaentrega.gestaoestrategicamiddle.repository.IndicadorPedidoEntreguePorClienteRepository;

@Component
public class IndicadorPedidoEntreguePorClienteConsumer {

	@Autowired
	IndicadorPedidoEntreguePorClienteRepository repository;

	@RabbitListener(queues = { "${queue.name.pedido.entregue.por.cliente.gestao.estrategica}" })
	public void receive(@Payload String mensagem) throws JsonProcessingException {
		IndicadorPedidoEntreguePorClienteDto indicadorDto = JsonUtil.gerarDto(mensagem,
				IndicadorPedidoEntreguePorClienteDto.class);
		repository.save(indicadorDto.getItens());
	}

}