package br.com.boaentrega.gestaoestrategicamiddle.dto;

import java.util.List;

import lombok.Data;

@Data
public class QuadroListDto {

	public List<QuadroDto> boards;

}
