package br.com.boaentrega.gestaoestrategicamiddle.config;

import java.util.List;

import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import br.com.boaentrega.core.dto.indicador.ItemPedidoEntreguePorClienteDto;

@Configuration
@EnableConfigurationProperties(RedisProperties.class)
public class RedisConfig {

	@Bean
	public RedisTemplate<String, List<ItemPedidoEntreguePorClienteDto>> redisTemplate(
			RedisConnectionFactory connectionFactory) {
		RedisTemplate<String, List<ItemPedidoEntreguePorClienteDto>> template = new RedisTemplate<>();
		template.setConnectionFactory(connectionFactory);
		return template;
	}
}