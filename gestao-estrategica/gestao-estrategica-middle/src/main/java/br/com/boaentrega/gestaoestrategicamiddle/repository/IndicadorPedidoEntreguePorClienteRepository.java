package br.com.boaentrega.gestaoestrategicamiddle.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import br.com.boaentrega.core.dto.indicador.ItemPedidoEntreguePorClienteDto;

@Component
public class IndicadorPedidoEntreguePorClienteRepository {

	private static final String QTDE_PEDIDO_ENTREGUE_POR_CLIENTE = "QTDE_PEDIDO_ENTREGUE_POR_CLIENTE";

	@Autowired
	private RedisTemplate<String, List<ItemPedidoEntreguePorClienteDto>> redisTemplate;

	public void save(List<ItemPedidoEntreguePorClienteDto> pedidoEntreguePorClienteDto) {
		redisTemplate.opsForValue().set(QTDE_PEDIDO_ENTREGUE_POR_CLIENTE, pedidoEntreguePorClienteDto);
	}

	public List<ItemPedidoEntreguePorClienteDto> findAll() {
		return redisTemplate.opsForValue().get(QTDE_PEDIDO_ENTREGUE_POR_CLIENTE);
	}

}
