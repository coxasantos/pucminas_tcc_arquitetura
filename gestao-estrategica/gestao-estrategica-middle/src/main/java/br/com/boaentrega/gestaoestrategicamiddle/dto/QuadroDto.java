package br.com.boaentrega.gestaoestrategicamiddle.dto;

import lombok.Data;

@Data
public class QuadroDto {

	private Long id;

	private String name;

}
