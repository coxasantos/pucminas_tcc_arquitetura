package br.com.boaentrega.gestaoestrategicamiddle.dto;

import lombok.Data;

@Data
public class ResponseQuadroDto {

	public QuadroListDto data;

	public int account_id;

}
