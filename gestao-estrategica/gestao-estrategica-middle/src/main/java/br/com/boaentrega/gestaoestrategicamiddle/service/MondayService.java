package br.com.boaentrega.gestaoestrategicamiddle.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.boaentrega.gestaoestrategicamiddle.dto.ResponseQuadroDto;

@Service
public class MondayService {

	@Value("${monday.url}")
	private String url;

	@Value("${monday.token}")
	private String token;

	public void enviar() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", token);
		headers.add("content-type", "application/json");
			
		String query = "{\"query\": \"query {boards (limit:1) {id name} }\"}";

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<ResponseQuadroDto> response = restTemplate.postForEntity(url, new HttpEntity<>(query, headers),
				ResponseQuadroDto.class);

		System.out.println(response);

//		restTemplate.exchange(url, HttpMethod.GET, request, new ParameterizedTypeReference<List<FornecedorDTO>>() {
//		}, params);

	}

}
