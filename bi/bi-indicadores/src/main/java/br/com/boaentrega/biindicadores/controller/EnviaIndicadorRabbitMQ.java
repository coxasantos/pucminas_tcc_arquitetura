package br.com.boaentrega.biindicadores.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.boaentrega.biindicadores.message.EnviaIndicadorRabbitMQService;
import br.com.boaentrega.core.dto.indicador.IndicadorPedidoEntreguePorClienteDto;

@RestController
@RequestMapping("indicadores/api/v1")
public class EnviaIndicadorRabbitMQ {

	@Autowired
	private EnviaIndicadorRabbitMQService service;

	private static final String DADOS_ENVIDADOS = "Os dados foram enviados";

	@PostMapping("/pedido/qtde_entregues_por_cliente")
	public String enviarIndicadorPedidoEntreguePorCliente(@RequestBody IndicadorPedidoEntreguePorClienteDto indicador)
			throws JsonProcessingException {
		service.enviarIndicadorPedidoEntreguePorCliente(indicador);
		return DADOS_ENVIDADOS;
	}

}