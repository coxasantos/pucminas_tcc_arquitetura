package br.com.boaentrega.biindicadores.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.boaentrega.biindicadores.config.QueueSender;
import br.com.boaentrega.core.dto.indicador.IndicadorPedidoEntreguePorClienteDto;
import br.com.boaentrega.core.util.JsonUtil;

@Service
public class EnviaIndicadorRabbitMQService {

	@Autowired
	private QueueSender queueSender;

	public void enviarIndicadorPedidoEntreguePorCliente(IndicadorPedidoEntreguePorClienteDto indicador)
			throws JsonProcessingException {
		queueSender.sendQueuePedidoEntreguePorCliente(JsonUtil.gerarJson(indicador));
	}

}
