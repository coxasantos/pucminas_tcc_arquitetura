package br.com.boaentrega.biindicadores.config;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(value = "SenderConfig")
public class SenderConfig {

	@Value("${queue.name.pedido.entregue.por.cliente.gestao.estrategica}")
	private String messagePedidoEntreguePorCliente;

	@Bean
	public Queue queuePedidoEntreguePorCliente() {
		return new Queue(messagePedidoEntreguePorCliente, true);
	}

}