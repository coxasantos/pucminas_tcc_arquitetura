package br.com.boaentrega.biindicadores;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BiIndicadoresApplication {

	public static void main(String[] args) {
		SpringApplication.run(BiIndicadoresApplication.class, args);
	}

//	@EventListener(ApplicationReadyEvent.class)
//	public void doSomethingAfterStartup() {
//		consultaService.consultarIndicadoresClientePorEstado();
//	}

}
