# Módulo BI

## Tecnologias

* CDH (Cloudera Distribution Hadoop) - 5.13.0
* PDI (Pentaho Data Integration) - 8.3.0.0
* RabbitMQ 3.9.11
* Cassandra 4.0.1
* Java 16
* Java 8 Pentaho e Hadoop

## bi-mock-rabbitmq
Mock responsável por enviar dados para o RabbitMQ (cliente e pedido)

* http://localhost:8080/mock-rabbitmq/api/v1/cliente/enviar
* http://localhost:8080/mock-rabbitmq/api/v1/pedido/enviar

## Downloads 

* Cloudera QuickStart VM
  https://1drv.ms/u/s!Am4PqLFJZ2-ai50Z8pgu__oavzCWUg

* Pentaho PDI
  https://sourceforge.net/projects/pentaho/files/Pentaho%208.3/client-tools/pdi-ce-8.3.0.0-371.zip/download

## Configurações realizadas para integração do Pentaho com CDH

* Documento utilizado para referência: 
  https://documents.pub/document/getting-started-with-pentaho-and-cloudera-quickstart-vm-overview-this-document.html

* Adicionar no hosts da máquina local "ip_vm_cdh quickstart.cloudera"
  EX: 127.0.0.1 quickstart.cloudera
   
* Na maquina virtual realizar o start do Cloudera Enterprise (Launch Cloudera Enterprise)

* Baixar o Shim correspondente ao CDH 5.13
  https://sourceforge.net/projects/pentaho/files/Pentaho%208.3/shims/pentaho-hadoop-shims-cdh513-package-8.3.2019.05.00-371-dist.zip/download.
  Extrair no diretório do Pentaho data-integration/plugins/pentaho-big-data-plugin/hadoop-configurations/cdh513/.
  No arquivo data-integration/plugins/pentaho-big-data-plugin/plugin.properties alterar a propriedade "active.hadoop.configuration=cdh513"

* Acessar o Hive do Cloudera Manager (http://quickstart.cloudera:7180/) e baixar os arquivos de configuração (Actions e Download Client Configuration).
  Os arquivos .xml devem ser extraídos para o diretório data-integration/plugins/pentaho-big-data-plugin/hadoop-configurations/cdh513/

* No arquivo hdfs-site.xml incluir a propriedade abaixo
``` 
  <property>
    <name>dfs.client.use.datanode.hostname</name>
    <value>true</value>
  </property>
```   
https://forums.pentaho.com/threads/166565-Issues-connecting-Cloudera-CDH5-(VM)-on-PDI/

* No arquivo mapred-site.xml incluir a propriedade abaixo:
```
  <property>
    <name>mapreduce.app-submission.cross-platform</name>
    <value>true</value>
  </property>
```
  https://help.hitachivantara.com/Documentation/Pentaho/9.2/Setup/Advanced_settings_for_connecting_to_a_Cloudera_cluster
  
* No arquivo hdfs-site.xml incluir a propriedade abaixo:
```
  <property>
    <name>dfs.client.datanode-restart.timeout</name>
    <value>30</value>
  </property>
  ```
* Criar um diretório no HDFS com o nome do usuário que executa o Pentaho. Para isso, na máquina virtual execute os seguintes comandos:
 ```
  sudo su - hdfs
  hdfs dfs -mkdir /user/nome_do_usuario
  hdfs dfs -chmod 777 /user/nome_do_usuario
  ``` 
    É possível confirmar o diretório criado através do HUE (http://quickstart.cloudera:8888/hue/)
  
* Na máquina local baixar os arquivos winutils.exe e hadoop.dll e configurar a variável de ambiente "HADOOP_HOME" apontando para o diretório dos arquivos baixados.
  https://stackoverflow.com/questions/41851066/exception-in-thread-main-java-lang-unsatisfiedlinkerror-org-apache-hadoop-io

* Atualizar a versão do Java do CDH para Java 8.
  https://gist.github.com/davideicardi/21aabb65faaa3c655770cf0ccbac6564
  
* Plugin protocolo AMQP para o Pentaho
  Extrair o plugin "ic-amqp-plugin" (../bi-pentaho-hadoop/ic-amqp-plugin.rar) no diretório do Pentaho:
  ../pdi-ce-8.3.0.0-371/data-integration/plugins/steps/ic-amqp-plugin.zip

## Imagem Docker Cassandra

* Baixa a imagem do Cassandra
```
  docker pull cassandra
``` 
* Executa uma instância na porta 9042
```
  docker run --name cassandra -p 9042:9042 -d cassandra
```

* Cria o keyspace "BOAENTREGA_BI"
```
  docker exec -it cassandra cqlsh
  create keyspace BOAENTREGA_BI with replication={'class':'SimpleStrategy','replication_factor':1}
  describe keyspaces
```
## Imagem Docker RabbitMQ

* Baixa a imagem do RabbitMQ
```
  docker pull rabbitmq
```
* Executa uma instância na porta 5672
```
  docker run -d --hostname localhost --name local-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management
```
  Management http://localhost:15672/
  
* As seguintes filas foram criadas: "bi_cliente_hadoop_integration" e "bi_pedido_hadoop_integration"
  
## Arquivos Pentaho

* Os arquivos estão na pasta ../bi-pentaho-hadoop/

#### Quantidade de clientes ativos por estado

* Job "cliente_rabbitmq.ktr" responsável por ler da fila "bi_cliente_hadoop_integration" a mensagem em Json e armazenar no HDFS do haddop.
* Job "cliente_por_estado_map_reduce_job.ktr" responsável por realizar o mapReduce quantidade de clientes ativos por estado e inserir no Cassandra o resultado.
  Arquivos que fazem parte do processo: "cliente_por_estado_transformacao.ktr", "cliente_por_estado_map.ktr", "cliente_por_estado_reduce.ktr" e "cliente_por_estado_cassandra.ktr"
  
#### Quantidade de pedidos entregues por cliente

* Job "pedido_rabbitmq.ktr" responsável por ler da fila "bi_pedido_hadoop_integration" a mensagem em Json e armazenar no HDFS do haddop.
* Job "pedido_entregue_por_cliente_map_reduce_job" Job responsável por realizar o mapReduce quantidade de pedidos entregues por cliente e inserir no Cassandra o resultado.
  Arquivos que fazem parte do processo: "pedido_entregue_por_cliente_transformacao.ktr", "pedido_entregue_por_cliente_map.ktr", "pedido_entregue_por_cliente_reduce.ktr" e "pedido_entregue_por_cliente_cassandra.ktr"
