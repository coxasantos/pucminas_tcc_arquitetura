package br.com.boaentrega.bimockrabbitmq.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.boaentrega.bimockrabbitmq.model.entity.ClienteEntity;
import br.com.boaentrega.core.enumeration.TipoClienteEnum;

public interface ClienteRepository extends JpaRepository<ClienteEntity, Long> {

	@Query("select cliente from ClienteEntity cliente where cliente.tipo=:tipoCliente")
	Page<ClienteEntity> findAllPorTipo(Pageable pageable, @Param("tipoCliente") TipoClienteEnum tipoCliente);

}
