package br.com.boaentrega.bimockrabbitmq.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "CID_CIDADE")
public class CidadeEntity implements Serializable {

	private static final long serialVersionUID = 8664148618662161139L;

	@Id
	@Column(name = "CID_ID")
	private Long id;

	@Column(name = "CID_NM_CIDADE")
	private String nome;

}