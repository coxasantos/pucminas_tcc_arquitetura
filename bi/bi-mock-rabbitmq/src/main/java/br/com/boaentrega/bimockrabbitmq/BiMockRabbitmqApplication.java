package br.com.boaentrega.bimockrabbitmq;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@EnableRabbit
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class BiMockRabbitmqApplication {

	public static void main(String[] args) {
		SpringApplication.run(BiMockRabbitmqApplication.class, args);
	}

}
