package br.com.boaentrega.bimockrabbitmq.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "PFI_PESSOA_FISICA")
@PrimaryKeyJoinColumn(name = "CLI_ID")
public class PessoaFisicaEntity extends ClienteEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "PFI_NOME")
	private String nome;

	@Column(name = "PFI_CPF")
	private String cpf;

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

}
