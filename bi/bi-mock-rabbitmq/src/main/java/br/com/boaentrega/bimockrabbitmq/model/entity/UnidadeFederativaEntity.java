package br.com.boaentrega.bimockrabbitmq.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "UNF_UNIDADE_FEDERATIVA")
public class UnidadeFederativaEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "UNF_ID")
	private Long id;

	@Column(name = "UNF_NM_UNIDADE_FEDERATIVA")
	private String nome;

	@ManyToOne
	@JoinColumn(name = "PAI_ID")
	private PaisEntity pais;
}
