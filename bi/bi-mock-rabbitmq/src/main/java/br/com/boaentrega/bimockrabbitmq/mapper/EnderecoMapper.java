package br.com.boaentrega.bimockrabbitmq.mapper;

import org.modelmapper.PropertyMap;

import br.com.boaentrega.bimockrabbitmq.model.entity.EnderecoClienteEntity;
import br.com.boaentrega.core.dto.EnderecoDto;

public class EnderecoMapper {

	private EnderecoMapper() {
		super();
	}

	protected static PropertyMap<EnderecoClienteEntity, EnderecoDto> getPropertyMap() {
		return new PropertyMap<EnderecoClienteEntity, EnderecoDto>() {
			protected void configure() {
				map().setCidade(source.getCidade().getNome());
				map().setPais(source.getPais().getNome());
				map().setUf(source.getUf().getNome());
			}
		};
	}

}
