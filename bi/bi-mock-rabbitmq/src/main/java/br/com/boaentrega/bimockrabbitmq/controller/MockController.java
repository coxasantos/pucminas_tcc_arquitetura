package br.com.boaentrega.bimockrabbitmq.controller;

import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.boaentrega.bimockrabbitmq.service.MockEnvioDadosClienteService;
import br.com.boaentrega.bimockrabbitmq.service.MockEnvioDadosPedidoService;

@RestController
@RequestMapping("mock-rabbitmq/api/v1")
public class MockController {

	@Autowired
	private MockEnvioDadosClienteService mockEnvioDadosCliente;

	@Autowired
	private MockEnvioDadosPedidoService mockEnvioDadosPedido;

	private static final String DADOS_ENVIDADOS = "Os dados foram enviados";

	@GetMapping("/cliente/enviar")
	public String enviarDadosCliente() throws JsonProcessingException {
		mockEnvioDadosCliente.enviarDados();
		return DADOS_ENVIDADOS;
	}

	@GetMapping("/pedido/enviar")
	public String enviarDadosPedido() throws JsonProcessingException, NoSuchAlgorithmException {
		mockEnvioDadosPedido.enviarDados();
		return DADOS_ENVIDADOS;
	}

}