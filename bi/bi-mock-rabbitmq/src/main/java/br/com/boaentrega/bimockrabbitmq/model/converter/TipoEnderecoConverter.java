package br.com.boaentrega.bimockrabbitmq.model.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.boaentrega.core.enumeration.TipoEnderecoEnum;

@Converter
public class TipoEnderecoConverter implements AttributeConverter<TipoEnderecoEnum, Integer> {

	@Override
	public Integer convertToDatabaseColumn(TipoEnderecoEnum attribute) {
		return attribute.getCodigo();
	}

	@Override
	public TipoEnderecoEnum convertToEntityAttribute(Integer codigo) {
		if (codigo == null) {
			return null;
		}
		return TipoEnderecoEnum.obterPorCodigo(codigo);
	}

}
