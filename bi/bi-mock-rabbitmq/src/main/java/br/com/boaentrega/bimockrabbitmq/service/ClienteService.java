package br.com.boaentrega.bimockrabbitmq.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.boaentrega.bimockrabbitmq.mapper.ClienteMapper;
import br.com.boaentrega.bimockrabbitmq.model.entity.ClienteEntity;
import br.com.boaentrega.bimockrabbitmq.repository.ClienteRepository;
import br.com.boaentrega.core.dto.ClienteDto;
import br.com.boaentrega.core.enumeration.TipoClienteEnum;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	public List<ClienteDto> obter50Clientes() {
		Pageable page = PageRequest.of(1, 50);
		Page<ClienteEntity> pageCliente = clienteRepository.findAllPorTipo(page, TipoClienteEnum.REMETENTE);
		return ClienteMapper.transformar(pageCliente.getContent());
	}

}
