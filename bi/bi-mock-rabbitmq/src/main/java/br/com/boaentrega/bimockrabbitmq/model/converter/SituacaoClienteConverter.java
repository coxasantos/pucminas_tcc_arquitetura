package br.com.boaentrega.bimockrabbitmq.model.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.boaentrega.core.enumeration.SituacaoClienteEnum;

@Converter
public class SituacaoClienteConverter implements AttributeConverter<SituacaoClienteEnum, String> {

	@Override
	public String convertToDatabaseColumn(SituacaoClienteEnum attribute) {
		return attribute.getValor();
	}

	@Override
	public SituacaoClienteEnum convertToEntityAttribute(String valor) {
		if (valor == null) {
			return null;
		}
		return SituacaoClienteEnum.obterPorValor(valor);
	}

}
