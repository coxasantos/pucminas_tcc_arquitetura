package br.com.boaentrega.bimockrabbitmq.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.PropertyMap;

import br.com.boaentrega.bimockrabbitmq.model.entity.ClienteEntity;
import br.com.boaentrega.bimockrabbitmq.model.entity.PessoaJuridicaEntity;
import br.com.boaentrega.core.dto.ClienteDto;
import br.com.boaentrega.core.dto.PessoaFisicaDto;
import br.com.boaentrega.core.dto.PessoaJuridicaDto;
import br.com.boaentrega.core.mapper.GenericModelMapper;

public class ClienteMapper {

	private ClienteMapper() {
		super();
	}

	@SuppressWarnings("rawtypes")
	public static ClienteDto transformar(ClienteEntity clienteEntity) {
		List<PropertyMap> lista = new ArrayList<>();
		lista.add(EnderecoMapper.getPropertyMap());

		if (clienteEntity instanceof PessoaJuridicaEntity) {
			return GenericModelMapper.transform(clienteEntity, PessoaJuridicaDto.class, lista);
		}

		return GenericModelMapper.transform(clienteEntity, PessoaFisicaDto.class, lista);

	}

	public static List<ClienteDto> transformar(List<ClienteEntity> clientes) {
		List<ClienteDto> clientesDto = new ArrayList<>();
		clientes.forEach(cliente -> clientesDto.add(ClienteMapper.transformar(cliente)));
		return clientesDto;
	}

}
