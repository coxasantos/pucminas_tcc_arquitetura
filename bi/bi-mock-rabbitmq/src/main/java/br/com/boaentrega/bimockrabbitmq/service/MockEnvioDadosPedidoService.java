package br.com.boaentrega.bimockrabbitmq.service;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.boaentrega.bimockrabbitmq.message.QueueSender;
import br.com.boaentrega.core.dto.ClienteDto;
import br.com.boaentrega.core.dto.PedidoDto;
import br.com.boaentrega.core.enumeration.StatusPedidoEnum;
import br.com.boaentrega.core.util.JsonUtil;

@Service
public class MockEnvioDadosPedidoService {

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private QueueSender queueSender;

	public void enviarDados() throws JsonProcessingException, NoSuchAlgorithmException {
		List<ClienteDto> clientes = clienteService.obter50Clientes();
		Random random = SecureRandom.getInstanceStrong();

		for (long i = 1; i < 150; i++) {
			PedidoDto pedido = new PedidoDto();
			pedido.setId(i);
			pedido.setIdCliente(clientes.get(random.nextInt(20)).getId());
			pedido.setStatus(StatusPedidoEnum.values()[random.nextInt(7)]);
			queueSender.sendQueuePedido(JsonUtil.gerarJson(pedido));
		}
	}

}
