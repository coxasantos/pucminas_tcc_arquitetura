package br.com.boaentrega.bimockrabbitmq.exception;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import javax.persistence.EntityNotFoundException;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.boaentrega.core.exception.InvalidException;

@RestController
@ControllerAdvice
public class ApiExceptionHandler {

	@ResponseStatus(INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	public InvalidException handleException(Exception ex) {
		return new InvalidException(ex.getMessage());
	}

	@ResponseStatus(BAD_REQUEST)
	@ExceptionHandler({ EntityNotFoundException.class })
	public InvalidException handleEntityNotFoundException(RuntimeException ex) {
		return new InvalidException(ex.getMessage());
	}

}
