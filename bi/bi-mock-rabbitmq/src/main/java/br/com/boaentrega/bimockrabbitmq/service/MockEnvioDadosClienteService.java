package br.com.boaentrega.bimockrabbitmq.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.boaentrega.bimockrabbitmq.message.QueueSender;
import br.com.boaentrega.core.dto.ClienteDto;
import br.com.boaentrega.core.util.JsonUtil;

@Service
public class MockEnvioDadosClienteService {

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private QueueSender queueSender;

	public void enviarDados() throws JsonProcessingException {
		List<ClienteDto> clientes = clienteService.obter50Clientes();
		for (ClienteDto cliente : clientes) {
			queueSender.sendQueueCliente(JsonUtil.gerarJson(cliente));
		}
	}

}
