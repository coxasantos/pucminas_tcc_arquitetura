package br.com.boaentrega.bimockrabbitmq.config;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(value = "SenderConfig")
public class SenderConfig {

	@Value("${queue.name.cliente}")
	private String messageCliente;

	@Value("${queue.name.pedido}")
	private String messagePedido;

	@Bean
	public Queue queueCliente() {
		return new Queue(messageCliente, true);
	}

	@Bean
	public Queue queuePedido() {
		return new Queue(messagePedido, true);
	}

}