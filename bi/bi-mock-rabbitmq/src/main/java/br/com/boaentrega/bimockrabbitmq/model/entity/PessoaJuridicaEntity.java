package br.com.boaentrega.bimockrabbitmq.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "PJU_PESSOA_JURIDICA")
@PrimaryKeyJoinColumn(name = "CLI_ID")
public class PessoaJuridicaEntity extends ClienteEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "PJU_RAZAO_SOCIAL")
	private String razaoSocial;

	@Column(name = "PJU_NOME_FANTASIA")
	private String nomeFantasia;

	@Column(name = "PJU_NR_CNPJ")
	private String cnpj;

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

}
