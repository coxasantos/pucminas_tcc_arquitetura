package br.com.boaentrega.bimockrabbitmq.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "PAI_PAIS")

public class PaisEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PAI_ID")
	private Long id;

	@Column(name = "PAI_NM_PAIS")
	private String nome;

}
