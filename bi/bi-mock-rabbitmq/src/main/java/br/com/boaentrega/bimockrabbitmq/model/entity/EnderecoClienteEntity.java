package br.com.boaentrega.bimockrabbitmq.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.boaentrega.bimockrabbitmq.model.converter.TipoEnderecoConverter;
import br.com.boaentrega.core.enumeration.TipoEnderecoEnum;
import lombok.Data;

@Data
@Entity
@Table(name = "ECL_ENDERECO_CLIENTE")
public class EnderecoClienteEntity implements Serializable {

	private static final long serialVersionUID = -8442581683041302166L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ECL")
	@SequenceGenerator(name = "SEQ_ECL", sequenceName = "SEQ_ECL", allocationSize = 1)
	@Id
	@Column(name = "ECL_ID")
	private Long id;

	@OneToOne
	@JoinColumn(name = "CLI_ID")
	private ClienteEntity cliente;

	@Convert(converter = TipoEnderecoConverter.class)
	@Column(name = "TEC_ID")
	private TipoEnderecoEnum tipo;

	@Column(name = "ECL_ID_CEP")
	private Integer cep;

	@Column(name = "ECL_NM_LOGRADOURO")
	private String logradouro;

	@Column(name = "ECL_NR_LOGRADOURO")
	private String numero;

	@Column(name = "ECL_DS_COMPLEMENTO")
	private String complemento;

	@Column(name = "ECL_DS_BAIRRO")
	private String bairro;

	@ManyToOne
	@JoinColumn(name = "CID_ID")
	private CidadeEntity cidade;

	@ManyToOne
	@JoinColumn(name = "UNF_ID")
	private UnidadeFederativaEntity uf;

	@ManyToOne
	@JoinColumn(name = "PAI_ID")
	private PaisEntity pais;

	@Column(name = "ECL_LATITUDE")
	private Double latitude;

	@Column(name = "ECL_LONGITUDE")
	private Double longitude;

}
