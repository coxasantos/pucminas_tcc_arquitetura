package br.com.boaentrega.bimockrabbitmq.model.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.boaentrega.bimockrabbitmq.model.converter.SituacaoClienteConverter;
import br.com.boaentrega.bimockrabbitmq.model.converter.TipoClienteConverter;
import br.com.boaentrega.core.enumeration.SituacaoClienteEnum;
import br.com.boaentrega.core.enumeration.TipoClienteEnum;
import lombok.Data;

@Data
@Entity
@Table(name = "CLI_CLIENTE")
@Inheritance(strategy = InheritanceType.JOINED)
public class ClienteEntity implements Serializable {

	private static final long serialVersionUID = 8796582728553618691L;

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CLI")
	@SequenceGenerator(name = "SEQ_CLI", sequenceName = "SEQ_CLI", allocationSize = 1)
	@Id
	@Column(name = "CLI_ID")
	private Long id;

	@Convert(converter = TipoClienteConverter.class)
	@Column(name = "TCL_ID")
	private TipoClienteEnum tipo;

	@Convert(converter = SituacaoClienteConverter.class)
	@Column(name = "CLI_ST_SITUACAO")
	private SituacaoClienteEnum situacao;

	@OneToOne(mappedBy = "cliente")
	private EnderecoClienteEntity endereco;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClienteEntity other = (ClienteEntity) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

}
