package br.com.boaentrega.bimockrabbitmq.message;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QueueSender {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private Queue queueCliente;

	@Autowired
	private Queue queuePedido;

	public void sendQueueCliente(String order) {
		rabbitTemplate.convertAndSend(this.queueCliente.getName(), order);
	}

	public void sendQueuePedido(String order) {
		rabbitTemplate.convertAndSend(this.queuePedido.getName(), order);
	}
}